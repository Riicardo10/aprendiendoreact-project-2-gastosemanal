import React, { Component } from 'react';
import Gasto from './Gasto';

class ListaGastos extends Component {
    render() {
        return (
            <div className='gastos-realizados'>
                <h2>Listado</h2>
                { Object.keys( this.props.lista ).map( key => (
                    <Gasto 
                        detalle={this.props.lista[key ]}
                        key={key} />
                ) ) }
                
            </div>
        );
    }
}

export default ListaGastos;