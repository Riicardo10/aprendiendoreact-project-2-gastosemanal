import React from 'react';
import {revisarPresupuesto} from '../helper'

export const Restante = (props) => {
    return ( 
        <div className={ revisarPresupuesto(props.presupuesto, props.restante) }> 
            Restante: ${props.restante}
        </div>
     );
}