import React, { Component } from 'react';

class Formulario extends Component {

    gasto = React.createRef();
    cantidad = React.createRef();

    crearGasto = (e) => {
        e.preventDefault();
        const gasto = {
            gasto: this.gasto.current.value.charAt(0).toUpperCase() + (this.gasto.current.value.slice(1)).toLowerCase(),
            cantidad: this.cantidad.current.value
        }
        this.props.agregarGasto( gasto );
        e.currentTarget.reset();
        document.getElementById('gasto').focus();
    }
    render() {
        return (
            <form onSubmit={this.crearGasto}>
                <h2>Agrega tus gastos...</h2>
                <div className="campo">
                    <label>Gasto</label>
                    <input id='gasto' ref={this.gasto} type="text" placeholder="Ej. transporte, comida..." className="u-full-width" required />
                </div>
                <div className="campo">
                    <label>Cantidad</label>
                    <input ref={this.cantidad} type="number" placeholder="Ej. 300" className="u-full-width" required />
                </div>
                <input type="submit" value="Agregar" className="button-primary u-full-width" />
            </form>
        );
    }
}

export default Formulario;