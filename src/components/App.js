import React, { Component } from 'react';
import {Header} from './Header';
import Formulario from './Formulario';
import ListaGastos from './ListaGastos';
import '../styles/App.css';
import Control from './Control';

class App extends Component {

  constructor (props) {
    super(props);
    this.state = {
      presupuesto: '',
      restante: '',
      gastos: {}
    }
  }

  componentDidMount() {
    this.validarPresupuesto();
  }

  validarPresupuesto = () => {
    let presupuesto = 0;
    do{
      presupuesto = parseInt( prompt( 'Cuál es tu presupuesto?', 10 ) ) || 0;
    } while( presupuesto <= 0 );
    this.setState( { 
      presupuesto, 
      restante: presupuesto
    } );
  }
  
  agregarGasto = (gasto) => {
    const gastos = {...this.state.gastos};
    gastos['gasto' + Date.now()] = gasto;
    this.setState( {
      gastos: gastos,
      restante: this.state.restante - gasto.cantidad
    } );
  }

  render() {
    return (
      <div className="App container">
        <Header titulo='Gasto semanal' />
        <div className='contenido-principal contenido'>
          <div className='row'>
            <div className='one-half column'>
              <Formulario 
                agregarGasto={this.agregarGasto}/>
            </div>
            <div className='one-half column'>
              <ListaGastos 
                  lista={this.state.gastos} />
              <Control 
                  presupuesto={this.state.presupuesto}
                  restante={this.state.restante} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
