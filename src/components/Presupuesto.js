import React from 'react';

export const Presupuesto = (props) => {
    return (
        <div className='alert alert-primary'>
            Presupuesto: ${props.presupuesto}
        </div>
    );
}